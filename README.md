## Parcours formation Développeur Web - Openclassrooms

Projet 4 - 30 heures

# Optimisez un site web existant ( La chouette agence )


Points sur lesquels travailler : 
- Analyse de l’état actuel de SEO du site fourni
- Amélioration du SEO du site
- Comparaison des résultats

Compétences évaluées :
- Assurer l'accessibilité d'un site web
- Écrire un code HTML et CSS maintenable
- Optimiser le référencement d'un site web
- Réaliser une recherche des bonnes pratiques en développement web
- Optimiser la taille et la vitesse d’un site web

Livrables :
- Le rapport d’analyse SEO du site, utilisant le modèle fourni, dans lequel on identifie clairement les 10 recommandations sélectionnées
- Le code source complet du site amélioré
- Un rapport d’optimisation comprenant une comparaison des résultats (y compris de la vitesse de chargement et l’accessibilité)
